<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Product;
use App\ClientOrder;
class BuyController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->query('search');

        return view('front.orderproduct', [
            'orders' => ClientOrder::all()
                ->where('id', 'LIKE', "%{$q}%")
                ->paginate($request->query('limit', pagenation_count))
        ]);
    }
    public function create(Product $product){
        return view('front.order',[
            'product' =>$product
        ]);
    }

    public function store(Request $request )
    {
        $clientorder = ClientOrder::create($request->all());

      return "your order done";
    }
    public function getOrders(Request $request)
    {
        return view('vendor.ordertable', [
            'orders' => ClientOrder::query()->paginate(pagenation_count)
        ]);
    }

    public function destroy(Order $order)
    {
        $order->delete();
        return  redirect(route('order.index'));
    }
}
