<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use App\Category;
use App\Product;
use DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function showproduct(){
        return view('front.home',[
         
        'categories' => Category::all(),
        'products' => Product::all()

        ]);
    }  
    public function search(Request $request){
        $q = $request->query('search');
        $categoryfilter = $request->query('category');
        $pricefilter = $request->query('price');
        return view('front.table',[ 
            'products' =>DB::table('products')->where('name', 'like', "%{$q}%")->paginate($request->query('limit', 5)),
            'productsFilterd' => Product::where('price', '=', $pricefilter)->orWhere('category_id', '=', $categoryfilter)
            ->paginate($request->query('limit', 5)),
            ]);
              
    }
}
