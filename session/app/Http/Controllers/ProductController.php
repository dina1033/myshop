<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\User;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use App\Traits\ProductTrait;

class ProductController extends Controller
{
    use ProductTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = $request->query('search');

        return view('admin.product.index', [
            'products' => Product::with(['category' , 'user' ]) 
                ->where('name', 'LIKE', "%{$q}%")
                ->paginate($request->query('limit', pagenation_count))
        ]);
    }
    public function getProducts(Request $request)
    {
        return view('admin.product.table', [
            'products' => Product::query()->paginate(pagenation_count)
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.product.create', [
            'products' => Product::all(),
            'categories'=> Category::all(),
            'users'=> User::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $file_name = $this->saveImage($request->file('img'),'images/products');
        //$request->merge([
        //    'img'  => $file_name
        //]);
        $product = Product::create([
        'name' => $request->name,
        'img'=> $file_name,
        'description'=> $request->description,
        'category_id'=> $request->category_id,
        'price'=> $request->price,
        'quantity'=> $request->quantity,
        'user_id'=> $request->user_id
        
        
        
        
        ]);

        return redirect(route('admin.products.show', $product));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.product.show', [
            'product'=> $product
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('admin.product.edit', [
            'product' => $product,
            'products' => Product::all(),
            'categories'=> Category::all(),
            'users'=> User::all()
            
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        
        $product->update($request->all());
        return redirect(route('admin.products.show', $product));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return  redirect(route('admin.products.index'));
    }

    
}
