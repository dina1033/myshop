<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use DB;
class SearchController extends Controller
{
    public function index(){
        return view('admin.search.index' );
    }
    public function search(Request $request){
        return view('admin.search.table',[
            'products' =>Product::where('name','LIKE','%'.$request->search."%")->get()
            ]);
        
              
    }
}
