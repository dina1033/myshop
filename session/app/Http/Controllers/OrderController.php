<?php

namespace App\Http\Controllers;
use App\Order;
use App\Client;
use App\User;
use App\Detail;
use App\Product;
use App\paymentMethod;
use App\Repository\Order\OrderRepositoryInterface;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    private $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('admin.order.index', [
            'orders' => $this->orderRepository->paginate($request)
            
        ]);
        
    }
    public function getOrders(Request $request)
    {
        return view('admin.order.table', [
            'orders' => Order::query()->paginate(pagenation_count)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.order.create', [
            'orders' => Order::all(),
            'clients'=> Client::all(),
            'users'=> User::all(),
            'products'=> Product::all(),
            'payments'=>paymentMethod::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = $this->orderRepository->create($request);
        return redirect(route('admin.orders.show',  $order));  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order )
    {
        $order->load('products');
        return view('admin.order.show', [
            'order'=> $order
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.order.edit', [
            'order' => $order,
            'orders' => Order::all(),
            'clients'=> Client::all(),
            'users'=> User::all(),
            'products'=> Product::all(),
            'payments'=>paymentMethod::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->merge([
            'user_id' => $request->user()->id
        ]);
        $order->update($request->all());
        $products=$request->get('products');
        $totalamount=0;
        foreach($products as $product){
            $quantity=$product['quantity'];
            $price=$product['price'];
            $total= $quantity * $price;
            $totalamount +=$total;
        }
        $order->total_amount =$totalamount;
        $order->save();
        $order->products()->sync($request->get('products'));
        $order->save();
        return redirect(route('admin.orders.show', $order));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return  redirect(route('admin.orders.index'));
    }
}
