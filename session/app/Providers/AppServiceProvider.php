<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repository\Order\OrderRepository;
use App\Repository\Order\OrderRepositoryInterface;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
