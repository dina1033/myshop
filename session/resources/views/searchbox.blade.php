
@extends('layouts.front.app')
@section('content')
<section class="all-products container-fluid mt-5">
     <div class="card-body">
       
            @livewire('search')
       
     </div>
   </section>
<div class="nov-row policy-home col-lg-12 col-xs-12 mt-5 mb-3" >
    <div class="nov-row-wrap row mb-">
                            <!-- begin /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->
                            <div class="nov-html col-xl-4 col-lg-4 col-md-4">
                                <div class="block">
                                    <div class="block_content">
                                        <div class="policy-row"><i class="noviconpolicy noviconpolicy-1"></i>
                                            <div class="policy-content">
                                                <div class="policy-name">Free Delivery From $ 250</div>
                                                <div class="policy-des">Sed ut perspiciatis unde omnis iste</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->

                            <!-- begin /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->
                            <div class="nov-html col-xl-4 col-lg-4 col-md-4">
                                <div class="block">
                                    <div class="block_content">
                                        <div class="policy-row"><i class="noviconpolicy noviconpolicy-2"></i>
                                            <div class="policy-content">
                                                <div class="policy-name">Money Back Guarantee</div>
                                                <div class="policy-des">Sed ut perspiciatis unde omnis iste natus</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->

                            <!-- begin /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->
                            <div class="nov-html col-xl-4 col-lg-4 col-md-4">
                                <div class="block">
                                    <div class="block_content">
                                        <div class="policy-row"><i class="noviconpolicy noviconpolicy-3"></i>
                                            <div class="policy-content">
                                                <div class="policy-name">Authenticity 100% guaranteed</div>
                                                <div class="policy-des">Sed ut perspiciatis unde omnis iste natus</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- end /var/www/demo.bestprestashoptheme.com/public_html/savemart/themes/vinova_savemart/modules/novpagemanage/views/source/html.tpl -->
                        </div></div>                        
                </div>
            </div>
        </section>
    </div>
<script src="{{ asset('js/app.js') }}"></script>
@livewireScripts 
@endsection   