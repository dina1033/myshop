@foreach($orders as $order)
    <tr>
        <td>{{ $order->id }}</td>
        <td>
            @if ($order->product)
                {{ $order->product->name }}</a>
             @endif
        </td>
        <td>{{ $order->quantity }}</td>
        <td>

            <button type="button" class="btn btn-danger delete" data-url="{{ route('order.destroy', $order) }}">Delete</button>
           
        </td>
    </tr>
@endforeach