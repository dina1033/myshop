@extends('layouts.app')

@section('title', 'create inventory')

@section('content')
<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">inventory</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item active">inventory</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
            <div class="container" style="margin:auto">

           
                <div class="d-flex mb-4">
                    <div>
                        <img class="mt-3" style="width: 140px; height: 129px;" src="{{asset('images/products/'.$product->img)}} }}">
                    </div>
                    <div>
                       <h2>{{$product->name}}</h2>
                        <h4>{{$product->price}}</h4>
                    </div>
                </div>
              
                <!-- /.row -->
                <form action="{{ route('admin.stocks.store' ) }}" method="post" enctype="multipart/form-data">
                @csrf  
                    <input type="hidden" value="{{$product->id}}"  name="product_id"  >
                        <div class="form-group ">
                            <label class=" col-sm-2 col-form-label " for="quantity">quantity</label>
                            <input class="  form-control" type="number" name="quantity" id="quantity" >
                        </div>
                        <div class="form-group">
                            <label class=" d-block " for="min-quantity">min-quantity</label>
                            <input class=" d-block form-control" type="number" name="min-quantity" id="min-quantity" >
                        </div>
                    
                    <div class="form-group">
                        <label class=" d-block " for="price">price</label>
                        <input class=" d-block form-control" type="number" name="price" id="price" >
                    </div>
                    <button class="btn btn-success mt-3">Create</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
@endsection