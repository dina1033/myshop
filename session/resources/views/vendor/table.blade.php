@foreach($stocks as $stock)
    <tr>
        <td>{{ $stock->id }}</td>
        <td>
            @if ($stock->product)
                {{ $stock->product->name }}
            @endif    
        </td>
        <td>{{ $stock-> quantity }}</td>
        
        <td>{{ $stock-> price }}</td>
        <td>                                      
            <button type="button" class="btn btn-danger delete" data-url="{{  route('admin.stocks.destroy', $stock) }}">Delete</button>
        </td>
    </tr>
@endforeach

