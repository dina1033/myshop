<li class="nav-item">
    <a href="{{ route('admin.dashboard') }}"
       class="nav-link {{ Route::currentRouteName() == 'admin.dashboard'? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer"></i>
        <p>
            Dashboard
        </p>
    </a>
</li>

@if (auth()->user()->is_admin==1)
<li class="nav-item">
    <a href="{{ route('admin.categories.index') }}"
       class="nav-link  {{ Route::currentRouteName() == 'admin.categories.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-th"></i>
        <p>
            Categories
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.products.index') }}" 
    class="nav-link  {{ Route::currentRouteName() == 'admin.products.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-th"></i>
        <p>
            Products
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.orders.index') }}" 
    class="nav-link  {{ Route::currentRouteName() == 'admin.orders.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-th"></i>
        <p>
            Orders
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.clients.index') }}"
    class="nav-link  {{ Route::currentRouteName() == 'admin.clients.index'? 'active' : '' }}">
        <i class="nav-icon fas fa-th"></i>
        <p>
            Clients
        </p>
    </a>
</li>
@endif
@if (auth()->user()->is_admin==0)
<li class="nav-item">
    <a href="{{ route('admin.search-for-product') }}"
    class="nav-link  {{ Route::currentRouteName() == 'admin.search-for-product'? 'active' : '' }}">
        <i class="nav-icon fa fa-cubes"></i>
        <p>
            Inventory
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('admin.stocks.index') }}"
    class="nav-link  {{ Route::currentRouteName() == 'stocks.index'? 'active' : '' }}">
        <i class="nav-icon fa fa-cubes"></i>
        <p>
           stock
        </p>
    </a>
</li>
@endif
<li class="nav-item">
    <a href="{{ route('order.index') }}"
    class="nav-link  {{ Route::currentRouteName() == 'order.index'? 'active' : '' }}">
        <i class="nav-icon fa fa-th"></i>
        <p>
        Client-order
        </p>
    </a>
</li>
