@extends('layouts.front.app')
@section('content')
<div class="content-wrapper mb-5">
        

        <!-- Main content -->
        <div class="content">
            <div class="container" style="margin:auto">          
                <div class="d-flex mb-4 justify-content-end">
                    
                    <div class="mr-4 mt-4">
                       <h2>{{$product->name}}</h2>
                        <h4>{{$product->price}}</h4>
                    </div>
                    <div  class="ml-4">
                        <img class="mt-3" style="width: 140px; height: 129px;" src="{{asset('images/products/'.$product->img)}} }}">
                    </div>
                </div>
              
                <!-- /.row -->
                <form action="{{ route('order.store' ) }}" method="post" enctype="multipart/form-data">
                @csrf  
                    <input type="hidden" value="{{$product->id}}"  name="product_id"  >
                        
                    
                    <div class="form-group">
                        <label class="  " for="quantity">quantity</label>
                        <input class="  form-control" type="number" name="quantity"  >
                    </div>
                    <button class="btn btn-success mt-3 mb-5" style="    min-height: 56px;font-size: 25px;margin:25px 492px 0">Create Order</button>
                </form>
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
    </div>
    @endsection     