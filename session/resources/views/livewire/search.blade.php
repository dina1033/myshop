<div class="container">
    <div class="row">
        <div class="col-md-12">
            
            <input type="text"  class="form-control" placeholder="Search" wire:model="searchTerm" />

            @foreach($products as $product)
            <div class="row">
                <div class="product col-4 ">
                    <div class="">
                        <img  style="width: 230px; height: 214px;" src="{{asset('images/products/'.$product->img)}}">
                    </div>
                    <div style ="margin-left: 20px;margin-top: 25px;">
                        <h2>{{$product->name}}</h2>
                        <h4>{{$product->price}}</h4>
                        <a class="buy btn btn-info" href="{{route('buyproduct' , $product)}}">Buy</a>
                    </div>
                    
                </div>
            </div>
            @endforeach
            {{ $products->links() }}
        </div>
    </div>
</div>