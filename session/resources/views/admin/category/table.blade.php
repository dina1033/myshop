@foreach($categories as $category)
    <tr>
        <td>{{ $category->id }}</td>
        <td>{{ $category->name }}</td>
        <td>
            @if ($category->parent)
                <a href="{{ route('admin.categories.show', $category->parent) }}" class="link-item">{{ $category->parent->name }}</a>
             @endif
        </td>
        <td>
            <a href="{{ route('admin.categories.show', $category) }}" class="btn btn-info">Show</a>
            <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-primary">Edit</a> 
            <button type="button" class="btn btn-danger delete" data-url="{{ route('admin.categories.destroy', $category) }}">Delete</button>
           
        </td>
    </tr>
@endforeach

