<?php

use Illuminate\Support\Facades\Route;
define('pagenation_count' ,5);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group([
    'prefix' => '/admin',
    'as' => 'admin.',
    'middleware' => ['auth' ]
], function () {
    
    Route::group([
        'middleware' => ['admin']
    ], function () {

    Route::group([
        'prefix' => 'clients',
        'as' => 'clients.'
    ], function () {
        Route::get('/get-clients', 'ClientController@getClients')->name('get-clients');
    });
    Route::resource('/clients', 'ClientController');



    Route::group([
        'prefix' => 'categories',
        'as' => 'categories.'
    ], function () {
        Route::get('/get-categories', 'CategoryController@getCategories')->name('get-categories');
    });
    Route::resource('/categories', 'CategoryController');



    Route::group([
        'prefix' => 'products',
        'as' => 'products.'
    ], function () {
        Route::get('/get-products', 'ProductController@getProducts')->name('get-products');
    });
    Route::resource('/products', 'ProductController');



    Route::group([
        'prefix' => 'orders',
        'as' => 'orders.'
    ], function () {
        Route::get('/get-orders', 'OrderController@getOrders')->name('get-orders');
    });
    
    Route::resource('/orders', 'OrderController');

    });


    Route::group([
        'prefix' => 'stocks',
        'as' => 'stocks.'
    ], function () {
        Route::get('/get-stocks', 'StockController@getStocks')->name('get-stocks');
    });
    Route::get('/', 'DashboardController')->name('dashboard');
    Route::get('/search', 'SearchController@index')->name('search-for-product');
    Route::get('/search-product', 'SearchController@search');
    
    Route::resource('/stocks', 'StockController');

    Route::get('/create-inventory/{product}' , 'StockController@create')->name('inventory');
});


Route::group([
    'prefix' => 'orders',
    'as' => 'orders.'
], function () {
    Route::get('/get-orders', 'Front\BuyControlle@getOrders')->name('get-product-orders');
});

Route::get('/buy/{product}', 'Front\BuyController@create')->name('buyproduct');
Route::resource('/order', 'Front\BuyController');

Route::get('/', function () {
    return view('searchbox');
});


Auth::routes();
Route::get('/clients/register' , 'Auth\RegisterController@clientRegisterForm')->name('client-register-form');
Route::post('/clients/register' , 'Auth\RegisterController@registerclient')->name('register-client');
Route::get('/clients/login' , 'Auth\LoginController@clientLoginForm')->name('client-login-form');
Route::post('/clients/login' , 'Auth\LoginController@loginclient')->name('login-client');
Route::get('/clients/logout' , 'Auth\LoginController@clientlogout')->name('logout-client');



