<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description')->nullable();
            $table->unsignedBigInteger('category_id')->index();
            $table->unsignedBigInteger('user_id')->nullable()->index();
            $table->Integer('price');
            $table->Integer('quantity');
            $table->string('img')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('category_id')
                ->on('category')
                ->references('id')
                ->cascadeOnDelete()
            ;
            $table->foreign('user_id')
                ->on('users')
                ->references('id')
                ->cascadeOnDelete()
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
