<?php
use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        foreach (range(0, 20) as $number) {
            Category::create([
                'name' => Str::random(5),
                'parent_id' => rand(1, count(Category::all())),
                'description' => Str::random(50)
            ]);
        }

    }

}
