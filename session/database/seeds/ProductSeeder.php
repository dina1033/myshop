<?php
use App\Product;
use App\Category;
use App\User;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        foreach (range(0, 20) as $number) {
            Product::create([
                'name' => Str::random(5),
                'category_id' => rand(1, count(Category::all())),
                'description' => Str::random(50),
                'price'=> rand(4, count(Product::all())),
                'quantity'=>rand(4, count(Product::all())),
                'img'=>Str::random(5),
                'user_id' => rand(1, count(User::all()))
            ]);
        }
    }
}
